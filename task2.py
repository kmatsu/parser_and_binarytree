# coding: UTF-8
import re
search_label = ''
class BinaryTree:
    def __init__(self, label):
        self.label = label
        
    def setNode(self, parent=None, left=None, right=None):
        self.left = left
        self.right = right
        self.parent = parent

    def inorderSearch(self, match_flg=True):
        if self.left:
            self.left.inorderSearch()
        else:
            if match_flg or self.parent == None:
                print("now, searching label name:" + self.label + ":{}".format(match_flg))
                if re.match(self.label, search_label):
                    print("label matched '{}'".format(search_label))
                    return
            if self.right:
                self.right.inorderSearch()
            else:
                if self.parent:
                    if self.parent.left == self:
                        self.parent.left = None
                    elif self.parent.right == self:
                        self.parent.right = None
                        match_flg=False
                    self.parent.inorderSearch(match_flg)
                else:   
                    print("no match!") 
                    return

if __name__ == '__main__':
    # create binary tree
    a = BinaryTree('a')
    b = BinaryTree('b')
    c = BinaryTree('c')
    d = BinaryTree('d')
    e = BinaryTree('e')
    f = BinaryTree('f')
    g = BinaryTree('g')
    i = BinaryTree('i')
    h = BinaryTree('h')
    
    # set node: parent, left, right
    a.setNode(b)
    b.setNode(f, a, d)
    c.setNode(d)
    d.setNode(b, c, e)
    e.setNode(d)
    f.setNode(None, b, g)
    g.setNode(f, None, i)
    i.setNode(g, h)
    h.setNode(i)

    # execute
    search_label = input("input search label:")
    f.inorderSearch()