# coding: UTF-8
import re

def parse_output(str):
    #with open('resources/whitelist.txt', 'r') as f:
    for whitelist in open('resources/whitelist.txt', 'r'):
        user_input = str
        whitelist = whitelist.rstrip('\r\n')
        whitelist = "^{}$".format(whitelist)
        reg = whitelist.replace("{word}", "(?P<text>.*)")
        result = re.match(reg, user_input)
        if result:
            return result.group('text')
    return False

if __name__ == "__main__":
    str = input("Please input a sentence.: ")
    result = parse_output(str)
    if result:
        print(result)
    else:
        print("no match!")